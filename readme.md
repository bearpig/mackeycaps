Mac Keycaps
================
This is a template for a 105 key mechanical keyboard (mine is Cherry G80 but should work for all/most) that has the following additions:-

1. Added standard mac icons.
2. Changed font to Helvetica Neue.
3. Centered all text or grid centered multiple icons.
4. Added ctrl variants for keys.
5. Change some keys to the abreviated versions (Page Up becomes pgup).
6. Change all 'labelled' keys to lowercase (personally thing it looks nicer).

It's based on the WASD Keyboards template and all text/icons are outlined. The format is .ai (saved as CS5 so you shouldn't have an issue with compatibility).


## License
This work is licensed under [**Creative Commons Attribution-NonCommercial 4.0 International License**](https://creativecommons.org/licenses/by-nc/4.0/). 
Do what you want with it as long as not commercial use.

## Why
Having used Mac keyboards for years, I finally switched to a Logitech solar-powered keyboard as I wanted wireless but without having to worry about batteries. This did be for a few years until it died, but after using a friends Cherry G80, I realised just how horrible typing for long periods on a really flat keyboard was.

So I bit the bullet and grabbed a Cherry G80 from Amazon. All good so far (and that familiar 'clack').
The problem was, I was so used to the layout of my older Mac keyboards, not having the icons on the keys was slightly annoying.

Enter WASD Keyboards - they will sell you a custom printed keycap set (or individual ones). So seeing as I'm already going to look at add in a few icons, why not relayout the whole thing with a nicer font, and more icons!


## Usage
1. Grab the .ai file from this repo.
2. Change whatever you want (colour doesn't matter at this stage) but as all are outlined, you'll need to retype any text if you want to reword anything.
2. Upload the layout (once happy) to whoever you're using to print the keycaps.
3. You'll need a program for remapping the keys - I'll update this readme with some info in the near future once I've tested a few.

## Images

![layout](https://bytebucket.org/bearpig/mackeycaps/raw/5860ac482a4642372c177e01297c5b0da5f85a22/images/mackeycaps.png)